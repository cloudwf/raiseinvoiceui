sap.ui.define([
    "com/sap/cp/dpa/invwpo/ReworkUI/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "com/sap/cp/dpa/invwpo/ReworkUI/Util/Util",
    "com/sap/cp/dpa/invwpo/ReworkUI/Util/formatter"

], function (BaseController, JSONModel, MessageBox, MessageToast, Util, formatter) {
    "use strict";

    return BaseController.extend("com.sap.cp.dpa.invwpo.ReworkUI.controller.DetermineApprover", {
        oFormatter: formatter,
        onInit: function () {
            this.getView().addEventDelegate({
                onBeforeShow: function(evt) {
                    this.openBusyDialog();
                    this._setDefaultTableModel();
                    this._getDetermineApprover();
                    this.closeBusyDialog();
                }.bind(this)
            }, this)
        },

        onNavigationBack: function(oEvent) {
            this.getView().getModel("DetermineApproverModel").setProperty("/data", []);
            this.getOwnerComponent().getTargets().display("TargetCreate", {
                fromTarget: "DetermineApprover"
            });
        },

        _setDefaultTableModel: function() {
            this.getView().setModel(new JSONModel({
                data: []
            }), "DetermineApproverModel");
        },

        _getDetermineApprover: function() {

            const fnSuccess = function (data) {
                var aTableData = this.getView().getModel("DetermineApproverModel").getData().data;
                for(var i = 0; i < data.Result.length; i++) {
                    var oResponse = {
                        IsApprovalRequired: data.Result[i].ApproverDetails.IsApprovalRequired,
                        Email: data.Result[i].ApproverDetails.Email,
                        UserID: (data.Result[i].ApproverDetails.UserId) ? data.Result[i].ApproverDetails.UserId : "",
                        UserGroup: (data.Result[i].ApproverDetails.UserGroup) ? data.Result[i].ApproverDetails.UserGroup : "",
                        ApproverLevel: this.sLevel,
                    };
                    // green status if IsApprovalRequired = true and UserGroup or UserId is returned
                    if(oResponse.IsApprovalRequired == true && 
                        (oResponse.UserID || oResponse.UserGroup)) {
                        
                        oResponse.StatusIcon = "sap-icon://status-completed";
                        oResponse.StatusState = "Success";
                    } else if(oResponse.IsApprovalRequired == false && 
                        (oResponse.UserID == "" || oResponse.UserGroup == "")) {  // green status if IsApprovalRequired = false and no UserGroup or UserId is returned
                        
                        oResponse.StatusIcon = "sap-icon://status-completed";
                        oResponse.StatusState = "Success";
                    } else if(oResponse.IsApprovalRequired == true && 
                        (oResponse.UserID == "" || oResponse.UserGroup == "")) { // red status if IsApprovalRequired = true but no lines are returned
                        
                        oResponse.StatusIcon = "sap-icon://status-negative";
                        oResponse.StatusState = "Error";
                    } else if(oResponse.IsApprovalRequired == undefined && 
                        (oResponse.UserID == "" || oResponse.UserGroup == "")){
                        
                        oResponse.StatusIcon = "sap-icon://status-negative";
                        oResponse.StatusState = "Error";
                    }
                    aTableData.push(oResponse);
                }
                this.getView().getModel("DetermineApproverModel").setProperty("/data", aTableData);
            }

            Util.sendDeterminateApproverRequest(fnSuccess, this);
        },

        
    });
}, /* bExport= */ true);
