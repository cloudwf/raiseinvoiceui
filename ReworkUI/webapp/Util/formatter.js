sap.ui.define([

], function () {
    "use strict";
    return {
        convertStringDateToDateObj: function(date) {
            if(date && typeof date === 'string') {

                var year = date.substring(0, 4);
                var month = date.substring(4, 6);
                var day = date.substring(6, 8);
                return new Date(year+"-"+month+"-"+day);
            }

            return date;
        },

        getLocaleFormat: function(date) {
            if(date && typeof date === 'string') {
                var year = date.substring(0, 4);
                var month = date.substring(4, 6);
                var day = date.substring(6, 8);
                return new Date(year+"-"+month+"-"+day).toLocaleDateString();
            } else if(date) {
                return date.toLocaleDateString();
            }
        },

        getHistoryTimeStampDateFormat: function(date) {
            return new Date(date).toLocaleString();
        },

        getSeverityIcon: function(sSeverity) {
            var icon;
            switch (sSeverity) {
                case 'High':
                    icon = 'sap-icon://error'
                    break;
            
                default:
                    icon = 'sap-icon://information'
                    break;
            }
            return icon;
        },

        getSeverityState: function(sSeverity) {
            var sState;
            switch (sSeverity) {
                case 'High':
                    sState = 'Error'
                    break;
            
                default:
                    sState = 'Information'
                    break;
            }
            return sState;
        },

        getApproverRequiredIcon: function(bRequired) {
            return (bRequired) ? 'sap-icon://accept' :  'sap-icon://decline'
        },

        getApproverRequiredState: function(bRequired) {
            return (bRequired) ? 'Success' :  'Error'
        }
    };
});