sap.ui.define([
    "com/sap/cp/dpa/invwpo/taskUI/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/core/format/DateFormat",
    "sap/m/BusyDialog",
    "sap/m/PDFViewer",
    "com/sap/cp/dpa/invwpo/taskUI/util/formatter"
], function (BaseController, JSONModel, MessageBox, MessageToast, DateFormat, BusyDialog, PDFViewer, formatter) {
    "use strict";
    var workflowId;
    return BaseController.extend("com.sap.cp.dpa.invwpo.taskUI.controller.Approve", {
        oFormatter: formatter,
        onInit: function () {
            setTimeout(function () {
                var oContextData = this.getModel().getData();
                this.configureView();

                // document service interaction
                this.oAttachmentsModel = new JSONModel();
                this.setModel(this.oAttachmentsModel, "attachmentsModel");
                this.getAttachments();
                this.getView().setModel(new JSONModel({ source: "" }), "pdfModel");
                var oVendorFieldsVisibilityModel = new JSONModel({
                    visible: false
                });
                this.getView().setModel(new JSONModel({
                    data: [{
                        "Name": "Default"
                    },
                    {
                        "Name": "High"
                    }]
                }), "commentSeverityModel");
                this.getView().getModel().setProperty("/commentSeverity", "Default");
                this.setMessageModelFromHistory();
                this.setUiHistoryModel();
                this.getView().setModel(oVendorFieldsVisibilityModel, "vendorFieldsVisibility");
                if (oContextData.invoiceDetails.headerDetail.invoiceId) {
                    this.showPDF();
                }
            }.bind(this), 1500);
        },
        onAfterRendering: function () {
            this._expandAllRowsInTreeTable();
        },
        onSourceValidationFailed: function (oEvent) {
            oEvent.preventDefault();
        },
        setUiHistoryModel: function () {
            const oInvoiceDetails = this.getView().getModel().getProperty("/invoiceDetails");
            const aUiHistory = oInvoiceDetails.uiHistory;
            this.getView().setModel(new JSONModel({
                data: aUiHistory
            }), "uiHistoryModel");
        },
        setMessageModelFromHistory: function () {
            const oInvoiceDetails = this.getView().getModel().getProperty("/invoiceDetails");
            const aUiHistory = oInvoiceDetails.uiHistory;
            if (aUiHistory && aUiHistory.length > 0) {
                const aSortedOnTimeStampHistory = aUiHistory.sort((x, y) => new Date(y.timeStamp) - new Date(x.timeStamp));
                if (aSortedOnTimeStampHistory && aSortedOnTimeStampHistory.length > 0) {
                    const oLatestHighSeverityComment = aSortedOnTimeStampHistory.find(e => e.severity === "High");

                    if (oLatestHighSeverityComment) {
                        this.getView().setModel(new JSONModel({
                            type: sap.ui.core.MessageType.Error,
                            message: oLatestHighSeverityComment.comment,
                            visible: true
                        }), "MessageModel");
                    } else {
                        this.getView().setModel(new JSONModel({
                            type: sap.ui.core.MessageType.None,
                            message: "",
                            visible: false
                        }), "MessageModel");
                    }

                } else {
                    this.getView().setModel(new JSONModel({
                        type: sap.ui.core.MessageType.None,
                        message: "",
                        visible: false
                    }), "MessageModel");
                }
            } else {
                this.getView().setModel(new JSONModel({
                    type: sap.ui.core.MessageType.None,
                    message: "",
                    visible: false
                }), "MessageModel");
            }
        },
        configureView: function () {

            var startupParameters = this.getComponentData().startupParameters;
            var approveText = this.getMessage("Approve");
            var declineText = this.getMessage("Decline");

            var oThisController = this;

            /**
             * APPROVE BUTTON
             */
            // Implementation for the approve action
            var oApproveAction = {
                sBtnTxt: approveText,
                onBtnPressed: function () {
                    var model = oThisController.getModel();
                    model.refresh(true);
                    var processContext = model.getData();

                    const oComment = processContext.comments;
                    var aUiHistory = oThisController.getView().getModel("uiHistoryModel").getProperty("/data");
                    aUiHistory.push({
                        userId: oThisController.getUserEmail(),
                        comment: ((oComment) ? oComment : ""),
                        role: "Approver",
                        action: "Approve",
                        actionText: "Approved",
                        severity: ((oComment) ? processContext.commentSeverity : "Default"),
                        timeStamp: new Date().toISOString()
                    });
                    processContext.uiHistory = aUiHistory;
                    // Call a local method to perform further action
                    oThisController._triggerComplete(
                        processContext,
                        startupParameters.taskModel.getData().InstanceID,
                        "approve"
                    );
                }
            };

            // Add 'Approve' action to the task
            startupParameters.inboxAPI.addAction({
                // confirm is a positive action
                action: oApproveAction.sBtnTxt,
                label: oApproveAction.sBtnTxt,
                type: "Accept"
            },
                // Set the onClick function
                oApproveAction.onBtnPressed);

            /**
            * Decline BUTTON
            */
            // Implementation for the decline action
            var oDeclineAction = {
                sBtnTxt: declineText,
                onBtnPressed: function () {
                    var model = oThisController.getModel();
                    model.refresh(true);
                    var processContext = model.getData();

                    if (processContext.comments === undefined || processContext.comments === "") {
                        sap.m.MessageBox.error(oThisController.getMessage("commentRequiredOnDecline"), {
                            title: "Error",
                            onClose: function () {
                                setTimeout(function () {
                                    oThisController.getView().byId("commentTextArea").focus();
                                }.bind(oThisController), 200);
                            }.bind(oThisController),
                            actions: sap.m.MessageBox.Action.CLOSE,
                        });

                        return;
                    }

                    var aUiHistory = oThisController.getView().getModel("uiHistoryModel").getProperty("/data");

                    aUiHistory.push({
                        userId: oThisController.getUserEmail(),
                        comment: processContext.comments,
                        role: "Approver",
                        action: "Decline",
                        actionText: "Declined",
                        severity: processContext.commentSeverity,
                        timeStamp: new Date().toISOString()
                    });

                    processContext.uiHistory = aUiHistory;
                    // Call a local method to perform further action
                    oThisController._triggerComplete(
                        processContext,
                        startupParameters.taskModel.getData().InstanceID,
                        "decline"
                    );
                }
            };

            // Add 'decline' action to the task
            startupParameters.inboxAPI.addAction({
                // confirm is a positive action
                action: oDeclineAction.sBtnTxt,
                label: oDeclineAction.sBtnTxt,
                type: "Reject"
            },
                // Set the onClick function
                oDeclineAction.onBtnPressed);
        },

        getUserEmail() {
            // Getting User details form IDP
            var user = sap.ushell.Container.getService("UserInfo");
            return user.getUser().getEmail();
        },

        _triggerComplete: function (processContext, taskId, approvalStatus) {

            var oThisController = this;

            this.openBusyDialog();

            $.ajax({
                // Call workflow API to get the xsrf token
                url: this._getWorkflowRuntimeBaseURL() + "/xsrf-token",
                method: "GET",
                headers: {
                    "X-CSRF-Token": "Fetch"
                },
                success: function (result, xhr, data) {

                    // After retrieving the xsrf token successfully
                    var token = data.getResponseHeader("X-CSRF-Token");

                    // form the context that will be updated
                    var oBasicData = {
                        context: {
                            invoiceDetails: {
                                uiHistory: processContext.uiHistory
                            },
                            internal: {
                                "comments": processContext.comments,
                                "approvalStatus": approvalStatus
                            }
                        },
                        "status": "COMPLETED"
                    };

                    $.ajax({
                        // Call workflow API to complete the task
                        url: oThisController._getWorkflowRuntimeBaseURL() + "/task-instances/" + taskId,
                        method: "PATCH",
                        contentType: "application/json",
                        // pass the updated context to the API
                        data: JSON.stringify(oBasicData),
                        headers: {
                            // pass the xsrf token retrieved earlier
                            "X-CSRF-Token": token
                        },
                        // refreshTask needs to be called on successful completion
                        success: function (result, xhr, data) {
                            oThisController._refreshTask();
                            oThisController.closeBusyDialog();
                        }

                    });
                }
            });

        },
        _getCPIBaseURL: function () {
            var componentName = this.getOwnerComponent().getManifestEntry("/sap.app/id").replaceAll(".", "/");
            var componentPath = jQuery.sap.getModulePath(componentName).replace("taskUI", "ReworkUI");
            return componentPath + "/CPI/http/";
        },

        _getDocumentServiceBaseURL: function () {
            var componentName = this.getOwnerComponent().getManifestEntry("/sap.app/id").replaceAll(".", "/");
            var componentPath = jQuery.sap.getModulePath(componentName);
            return componentPath + "/docservice/";
        },

        _getWorkflowRuntimeBaseURL: function () {
            var componentName = this.getOwnerComponent().getManifestEntry("/sap.app/id").replaceAll(".", "/");
            var componentPath = jQuery.sap.getModulePath(componentName);
            return componentPath + "/workflowruntime/v1";
        },

        // Request Inbox to refresh the control once the task is completed
        _refreshTask: function () {
            var taskId = this.getComponentData().startupParameters.taskModel.getData().InstanceID;
            this.getComponentData().startupParameters.inboxAPI.updateTask("NA", taskId);
            console.log("task is refreshed");
        },

        _expandAllRowsInTreeTable: function () {
            var oTreeTable = this.getView().byId("TreeTable");
            oTreeTable.expandToLevel(3); //number of the levels of the tree table.
        },

        /**
         * DOCUMENT SERVICE INTEGRATION
         */
        showPDF: function () {
            var oContextData = this.getModel().getData();
            var oPDFModel = this.getView().getModel("pdfModel");
            var oHtml = this.getView().byId("attachmentIFrame");

            var sURL = this._getCPIBaseURL() + "GetAttachmentFromS4?invoiceId=" + oContextData.invoiceDetails.headerDetail.invoiceId;

            var oSettings = {
                url: sURL,
                method: "GET"
            };

            $.ajax(oSettings)
                .done(function (result) {
                    var oJSONResult = JSON.parse(result);
			        if (oJSONResult) {
                        oPDFModel.setProperty("/source", oJSONResult.EV_LINK)
                        oHtml.setContent("<iframe src='" + oJSONResult.EV_LINK + "' height='700' width='100%'></iframe>");
                    }
                }.bind(this))
                .fail(function (err) {
                    if (err !== undefined) {
                    } else {
                        MessageToast.show("Unknown error!");
                    }
                });
        },

        getAttachments: function () {
            // get workflow ID
            var oModel = this.getView().getModel();
            workflowId = oModel.getData().headerInstanceId;

            var sUrl = this._getDocumentServiceBaseURL() + "workflowmanagement/NonPOVendorInvoiceDocuments/" + workflowId + "?succinct=true";
            /* TODO
            var oSettings = {
                "url": sUrl,
                "method": "GET",
                // "async": false
            };
            var oThisController = this;

            $.ajax(oSettings)
                .done(function (results) {
                    oThisController._mapAttachmentsModel(results);
                })
                .fail(function (err) {
                    if (err !== undefined) {
                        var oErrorResponse = $.parseJSON(err.responseText);
                    } else {
                        MessageToast.show("Unknown error!");
                    }
                });
                */
        },

        // assign data to attachments model
        _mapAttachmentsModel: function (data) {
            this.oAttachmentsModel.setData(data);
            this.oAttachmentsModel.refresh();
            console.log(this.oAttachmentsModel);
        },

        // formatting functions
        formatTimestampToDate: function (timestamp) {
            var oFormat = DateFormat.getDateTimeInstance();
            return oFormat.format(new Date(timestamp));
        },
        // File length  
        formatFileLength: function (fileSizeInBytes) {
            var i = -1;
            var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
            do {
                fileSizeInBytes = fileSizeInBytes / 1024;
                i++;
            } while (fileSizeInBytes > 1024);

            return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
        },
        // Function to download the attachment
        formatDownloadUrl: function (objectId) {
            return this._getDocumentServiceBaseURL() + "workflowmanagement/NonPOVendorInvoiceDocuments/"
                + workflowId + "?objectId=" + objectId + "&cmisselector=content";
        },
        // Function to convert the date format
        formatDate: function (date) {
            const dateFormat = sap.ui.core.format.DateFormat.getDateInstance({ pattern: "MMMM dd YYYY" });
            var formattedDate = dateFormat.parse(date);
            formattedDate = dateFormat.format(formattedDate);
            return formattedDate;
        }
    });
});