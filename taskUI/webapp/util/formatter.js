sap.ui.define([

], function () {
    "use strict";
    return {
        getLocaleFormat: function(date) {
            return new Date(date).toLocaleString();
        },

        getSeverityIcon: function(sSeverity) {
            var icon;
            switch (sSeverity) {
                case 'High':
                    icon = 'sap-icon://error'
                    break;
            
                default:
                    icon = 'sap-icon://information'
                    break;
            }
            return icon;
        },

        getSeverityState: function(sSeverity) {
            var sState;
            switch (sSeverity) {
                case 'High':
                    sState = 'Error'
                    break;
            
                default:
                    sState = 'Information'
                    break;
            }
            return sState;
        }
    };
});